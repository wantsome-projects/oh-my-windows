#! /usr/bin/env python
try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup


setup(
    name="oh_my_windows",
    version="0.1.0",
    description="Wantsome Project",
    long_description=open("README.md").read(),
    author="Wansome - Python Seria 2",
    url="https://gitlab.com/wantsome-projects/hunt-the-wumpus",
    packages=["oh_my_windows"],
    entry_points={
        'console_scripts': [
            'hunt-the-wumpus = oh_my_windows.cmdline:main',
        ],
    },
    requires=open("requirements.txt").readlines(),
)
